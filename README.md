# Documentação da API Pública do GestPrev

## Versão atual
Esta API encontra-se atualmente na versão 1.0, portanto por padrão todas requisições serão baseadas nesta versão.

## Formato dos dados
Todas as informações da API estão dispostas no formato [JSON](http://json.org), pois trata-se de um formato padrão, eficiente e com implementação disponível na maioria das linguagens de programação.

## Acesso
Para se comunicar com a API, deve-se utilizar como base o seguinte domínio:

> https://[nome_do_municipio].gestprev.com.br/public_api

Exemplo:

> https://nome_do_municipio.gestprev.com.br/public_api

Todas as `URLs` e `Recursos` citados nesta documentação devem ser consideradas um caminho relativo da URL base.

### Autenticação

A API é protegida contra acessos indevidos através do uso de `Tokens de Autenticação`. É um código que deve ser gerado na interface administrativa do GestPrev e posteriormente utilizado em todas requisições feitas na API.

#### Obtendo o Token

Primeiramente acesse o cadastro do RPPS, e no menu `API Keys` crie um novo token:

> https://nome_do_municipio.gestprev.com.br/rpps/api_keys

![API Keys](gestprev_api_keys.png)

#### Utilizando o Token

Para que a requisição seja autorizada deve-se informar o código gerado no Cabeçalho HTTP `Authorization` ([Seção 14.8 da Especificação HTTP](http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)). O valor do cabeçalho deve obrigatoriamente possuir o seguinte formato:

> Token token=0123456789abcdefgh0123456789abcd

Assim formando o cabeçalho completo:

> Authorization: Token token=0123456789abcdefgh0123456789abcd

Caso o token não seja informado, ou possua formato/valor incorreto, a API retornará resposta HTTP com `Status 401 - Unauthorized` ([Seção 10.4.2 da Especificação HTTP](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)), além da mensagem `HTTP Token: Access denied.` no corpo da resposta.

## Funcionalidades

## UUID

Todos registros possuem identificador único no formato [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier)

### Valores vazios ou em branco

Atributos que não possuem dados serão tratados com o valor `null`, ou com valor `[]` (array vazio):

```json
"data_emissao": null,
"dependentes": []
```

### Parâmetros

A API aceita na sua URL parâmetros do tipo `Query String`, que são informados após o caractere `?`. Exemplo:

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?page=2

Também é possível utilizar vários parâmetros simultaneamente para obter funilar os dados:

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?page=2&filter[sexo]=masculino

Os possíveis parâmetros estão documentados nas seções abaixo.

### Paginação

Todos os conjuntos de dados retornados pela API recebem tratamento de paginação de dados, para facilitar a visualização de dados e não comprometer a comunicação quando existem muitos itens a serem transferidos.

O parâmetro `page` deve ser informado para solicitar uma página específica do conjunto de dados.

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?page=500

Quando não informado, a API assume `page=1` por padrão.

No JSON de resposta, o atributo `meta` possui informações referentes à paginação (quando necessário), que podem ser usadas pelo software cliente:

```json
"meta": {
    "total_pages": 1740,
    "previous_page": 499,
    "current_page": 500,
    "next_page": 501
  }
```

### Filtros

De funcionamento similar à paginação, a API permite o filtrar os dados solicitados. Este filtro deve possuir o seguinte formato:

```
filter[atributo]=valor
```

Exemplo:

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?filter[sexo]=masculino

Os filtros trabalham comparando o valor no atributo informado. Os possíveis filtros estão documentados nos respectivos recursos.

## Recursos

Entende-se como recursos todas as entidades presentes e disponibilizadas pela API:

- Servidores
    - Dependentes
    - Tempos de contribuição outro RPPS
    - Tempos de contribuição RGPS
    - Tempos de contribuição sem certidão
- Dependentes
- Atualizações Cadastrais

### Servidores
Fornece dados pessoais, trabalho, endereço, contato, tempos de contribuição e dependentes.

Para solicitar todos servidores: 
> https://nome_do_municipio.gestprev.com.br/public_api/servidores

Para solicitar um servidor específico:
> https://nome_do_municipio.gestprev.com.br/public_api/servidores/4acdc964-6c22-4d37-993f-00ccea022dfc

#### Retorno

```json
{
  "servidores": [
    {
      "id": "00000000-0000-0000-0000-000000000000",
      "nome": "NOME DO SERVIDOR",
      "nome_mae": "NOME DA MAE",
      "nome_pai": "NOME DO PAI",
      "data_nascimento": "1972-04-27",
      "cpf": "12345678901",
      "pasep_pis_nit": "12345678901",
      "sexo": "Masculino",
      "estado_civil": "Solteiro(a)",
      "tipo_deficiencia": "Nenhuma",
      "escolaridade": "Superior incompleto",
      "email": "email@email.com",
      "endereco": {
        "cep": "12345678",
        "logradouro": {
          "tipo": "Rua",
          "descricao": "RUA NOME"
        },
        "complemento": null,
        "numero": "163",
        "bairro": "Vila Sem Nome",
        "municipio": "Nome do Municipio",
        "uf": "SP",
        "tipo_imovel": "Próprio"
      },
      "telefones": {
        "fixo": "1112345678",
        "celular": "11912345678"
      },
      "data_ingresso_servico_publico": "1996-07-08",
      "documento_ingresso_servico_publico": "PORTARIA 11/80",
      "email_institucional": null,
      "ctps": {
        "numero": "00000",
        "serie": "000",
        "data_emissao": "1991-08-22",
        "uf_expedicao": "SP"
      },
      "titulo_eleitor": {
        "numero": "123456789012",
        "zona": "111",
        "secao": "222",
        "uf_expedicao": "SP"
      },
      "dependentes": [
        {
          "id": "00000000-0000-0000-0000-000000000000",
          "nome": "NOME DEPENDENTE",
          "nome_mae": "NOME MAE",
          "nome_pai": null,
          "data_nascimento": "1972-10-05",
          "cpf": "12345678901",
          "sexo": null,
          "estado_civil": null,
          "tipo_deficiencia": "Nenhuma",
          "escolaridade": null,
          "email": null,
          "endereco": {
            "cep": "12345678",
            "logradouro": {
              "tipo": "Rua",
              "descricao": "RUA NOME"
            },
            "complemento": null,
            "numero": "163",
            "bairro": "Vila Sem Nome",
            "municipio": "Nome do Município",
            "uf": "SP",
            "tipo_imovel": "Próprio"
          },
          "telefones": {
            "fixo": "",
            "celular": ""
          }
        }
      ],
      "tempos_contribuicoes_outros_rpps": [],
      "tempo_contribuicao_rgps": null,
      "tempos_contribuicoes_sem_certidao": [
        {
          "cnpj": "12345678901234",
          "nome_cargo_efetivo": "AJUDANTE GERAL",
          "data_inicio": "1995-03-01",
          "data_fim": "1996-05-31",
          "tempo_liquido_anos": 1,
          "tempo_liquido_meses": 3,
          "tempo_liquido_dias": 0,
          "numero_dias": 455,
          "magisterio_exclusivo": "Não",
          "regime": "privado",
          "nome_empregador": "EMPRESA LTDA ME",
          "acumulavel": "Não"
        }
      ]
    }
   ]
}
```

#### Filtro de servidores

**Sexo**

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?filter[sexo]=masculino

Valores aceitos: `masculino`, `feminino`, `null`

**Data de Nascimento**

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?filter[data_nascimento]=1954-01-07

Formato da data [ISO8601](http://www.w3.org/TR/NOTE-datetime): `ANO-MES-DIA`

**CPF**

> https://nome_do_municipio.gestprev.com.br/public_api/servidores?filter[cpf]=00000000000

Deve ser um CPF válido com 11 dígitos, caso contrário o fitro é ignorado.

### Atualizações Cadastrais
Fornece dados da atualização cadastral de um servidor, contendo o histórico das alteração de valores antigos e novos.

> https://nome_do_municipio.gestprev.com.br/public_api/servidores/4acdc964-6c22-4d37-993f-00ccea022dfc/atualizacoes_cadastrais

#### Retorno

```json
{
  "atualizacoes_cadastrais": [
    {
      "dados_pessoais": [
        {
          "table": {
            "data_alteracao": "2015-03-13T09:09:18.483-03:00",
            "tipo_evento": "Atualização",
            "modelo_alterado": "pessoa",
            "reponsavel_alteracao": "4acdc964-6c22-4d37-993f-00ccea022dfc",
            "alteracoes": [
              {
                "campo": "naturalidade_municipio_id",
                "valor_antigo": "Não informado",
                "valor_novo": "São Paulo"
              }
            ]
          }
        }
      ]
    }
  ]
}
```

### Dependentes
Fornece dados de todos dependentes disponíveis.

#### Retorno

```json
{
  "dependentes": [
    {
      "id": "00000000-0000-0000-0000-000000000000",
      "nome": "NOME DEPENDENTES",
      "nome_mae": "NOME MAE",
      "nome_pai": "NOME PAI",
      "data_nascimento": "2007-02-23",
      "cpf": "12345678901",
      "sexo": "Feminino",
      "estado_civil": "Solteiro(a)",
      "tipo_deficiencia": "Nenhuma",
      "escolaridade": "Fundamental incompleto",
      "email": "email@email.com",
      "endereco": {
        "cep": "12345678",
        "logradouro": {
          "tipo": "Rua",
          "descricao": "RUA SEM NOME"
        },
        "complemento": null,
        "numero": "159",
        "bairro": "BAIRRO SEM NOME",
        "municipio": "Nome do Município",
        "uf": "SP",
        "tipo_imovel": "Próprio"
      },
      "telefones": {
        "fixo": "0012345678",
        "celular": "00912345678"
      }
    }
  ]
}
```
